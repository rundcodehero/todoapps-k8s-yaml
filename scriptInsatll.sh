#!/bin/bash
kubectl create ns production
kubectl create ns staging
kubectl create ns development
kubectl create ns argocd
kubectl create ns kube-monitoring
kubectl create ns kube-logging

cd prod
kubectl apply -f .
cd ..
#cd stag
#kubectl apply -f .
#cd ..
#cd dev
#kubectl apply -f .
#cd ..
cd ingress
kubectl apply -f .
cd ..
cd  argocd
kubectl apply -f .


echo "deploy success"
